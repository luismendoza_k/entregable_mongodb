package com.proyectos.proyecto01.servicio.impl;

import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import com.proyectos.proyecto01.servicio.repositorios.RepositorioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioClientes;

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioClientes.findAll();
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioClientes.insert(cliente);
    }
    @Override
    public Cliente obtenerCliente(String documento) {
        System.out.println("Cliente : " + documento);
        final Optional<Cliente> quizasCliente = this.repositorioClientes.findById(documento);
        return quizasCliente.isPresent()? quizasCliente.get() : null;
    }

    @Override
    public void guardarCliente(String documento, Cliente cliente) {
        //Si no existe lo crea, si existe lo  remplaza completamente
        this.repositorioClientes.save(cliente);
    }
    @Override
    public void borrarCliente(String documento) {
        this.repositorioClientes.deleteById(documento);
    }

    @Override
    public void emparcharCliente(Cliente cliente) {
        throw new UnsupportedOperationException("No implementado");
    }

    @Override
    public void borrarTodo() {
        throw new UnsupportedOperationException("No implementado");
    }

    @Override
    public void agregarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.codigoCuentas.add(cuenta.numero);
        this.repositorioClientes.save(cliente);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = this.obtenerCliente(documento);
        List<String> cuentas = cliente.codigoCuentas;

        return cuentas;
    }
}
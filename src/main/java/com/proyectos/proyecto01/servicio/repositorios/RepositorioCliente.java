package com.proyectos.proyecto01.servicio.repositorios;

import com.proyectos.proyecto01.modelo.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

//Requiere la clase modelo y el tipo de dato de la llave
public interface RepositorioCliente extends MongoRepository<Cliente,String> {


}
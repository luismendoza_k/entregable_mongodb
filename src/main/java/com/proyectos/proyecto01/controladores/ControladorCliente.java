package com.proyectos.proyecto01.controladores;

import com.proyectos.proyecto01.Util.Rutas;
import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
//@RequestMapping("${apirest.url}")
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    //GET http:localhost:9000/api/v1/clientes --> Lista de clientes
    // Se añade Paginacion
    //GET http:localhost:9000/api/v1/clientes ?pagina=7&cantidad=3
    @GetMapping
    public List<Cliente> obtenerClientes(@RequestParam int pagina,@RequestParam int cantidad){
        return this.servicioCliente.obtenerClientes(pagina-1, cantidad);
    }


    // GET http:localhost:9000/api/v1/clientes/{documento} --- > Obtiene un cliente
    @GetMapping("/{documento}")
    public ResponseEntity<Cliente> obtenerCliente(@PathVariable String documento){

        try {
            return ResponseEntity.ok().body(this.servicioCliente.obtenerCliente(documento));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }

    }

    //POST http:localhost:9000/api/v1/clientes + DATOS --- >Inserta un cliente
    // 200 ok, Location: http://localhost:9000/api/v1/cliente/123456789
    // Clase Link {rel, url}
    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente){
        // ObtenerCliente(documento) --> http:localhost:9000/api/v1/clientes/12345678 (Link {rel,url)}
        //Link.url --> Location



        this.servicioCliente.insertarClienteNuevo(cliente);

        final Link enlaceEsteDocumento = linkTo(methodOn(ControladorCliente.class).obtenerCliente(cliente.documento)).withSelfRel();
        return ResponseEntity.ok().location(enlaceEsteDocumento.toUri()).build();

    }

    //PUT http:localhost:9000/api/v1/clientes/{documento} + DATOS --> Reemplazar un cliente
    //En caso no coincida el nombre de la ruta con el nombre de la variable Java
    //Reemplaza todo el objeto Json
    @PutMapping("/{documento}")
    public ResponseEntity reemplazarCliente(@PathVariable(name= "documento") String documentoId , @RequestBody Cliente cliente ){
        try {
            cliente.documento= documentoId;
            this.servicioCliente.guardarCliente(documentoId, cliente);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();

    }

    @PatchMapping("/{documento}")
    public ResponseEntity empacharCliente(@PathVariable(name= "documento") String documentoId , @RequestBody Cliente cliente ){

        try {
            cliente.documento= documentoId;
            this.servicioCliente.emparcharCliente(cliente);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarCliente(@PathVariable(name="documento") String documento){
        try {
            this.servicioCliente.borrarCliente(documento);
        }catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.OK);
        }
    }

    @DeleteMapping
    public void borrarTodo(){
        this.servicioCliente.borrarTodo();
    }
}
